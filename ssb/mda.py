import markdown

fileName = "../notebook.history.md"

postTemplate = "./startbootstrap-clean-blog/t-mypost.html"

tmpFile = "/tmp/md0124.html"


def transferMarkdown():
    html = None

    with open(fileName, 'r') as f:

        text = f.read()
        html = markdown.markdown(text)

        with open(tmpFile, 'w') as w:
            w.write(html)

            pass

        pass

    return html


from jinja2 import Environment, FileSystemLoader, select_autoescape

def fillTemplate():
    env = Environment(
        loader = FileSystemLoader(searchpath = "./"),
        autoescape = select_autoescape(default_for_string = True)
    )

    template = env.get_template(postTemplate)

    html = transferMarkdown()

    renderred = template.render(notebook_of_history_html  = html)

    return renderred


if __name__ == "__main__":
    h = fillTemplate()


    htmlFile = "./startbootstrap-clean-blog/notebook.html"
    with open(htmlFile, 'w') as f:

        f.write(h)

