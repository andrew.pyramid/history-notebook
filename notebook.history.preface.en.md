---
Title: English Preface for "Notebook of History"
Date:  2021-11-22
Category: History
---



[Chinese Version: ](/index)
[Chinese Version: ](/y10m/escape.pyramid.preface)


It is easier for an elephant to go through the eye of a needle 
than for a corrupted man to enter the kingdom of Heaven.

The gate of hell is in the opposite,
filthy people come out in groups.

We're going to focus on how to build a hell.

What's the fuel feeding up the ecosystem in the hell?

They read salacious history books and grew up corrupted.

The history books of the hell are full of cheatings,
forbid to be falsified, only allow proofs.
It can't be discussed, only allow repeatings,
It can't be studied, only promote propagandas...
It always been forcefied by violences, and forced sink underground of the hell.

So, who write history books for CCP,
who intentionally distort human records of history,
who sell those books,
who teach those books,
who propagand lies,
you're cursed and wish you live in the hell you build forever.

CCP's books equals to the manuals of how to build hell,
and we're going to study the macro structure of the hell: pyramid.

How to build an hell?

Firstly, build a pyramid structure of power,
setup layers of dictatorship,
make it the single geometry of the society,
this pyramid is the macro framework of the hell,
by theory of geometry it can be proved the following is going to happen:

    - Destroyed common sense of logic, confused truth and false, so people
      can't judge the good from the bad.

    - Ruin of arts, appreciation of the beauty not based on human figure, but
      beasts eating flesh of human beings.

    - No worship of God, and the rightous person been crucified.
    - enjoyments of torture, cruelties and abusive sex depression, 
      it's going to be rewards on steps on the pyramid.

    - Lost sense of science, people utilize technologies to take advantage of
      their colleagues.

  - 扭曲科学，把科学技术变为囚禁人类的牢笼，师夷长技以制夷。
  - 洗脑愚民，让奴隶们拥护奴隶制。
  - 埋送自由，埋葬言论自由，从而扼杀学术自由和信仰自由。
  - 反向法制，培育愚民，再以法制民，却放纵禽兽。
  - 退化语言，给吃人的禽兽们起一个理想的名字：祖国。
  - ...

人类爬上权力的金字塔后就会变身为_爬虫_，好好学习，天天向上。
而爬虫们当然会污染环境，会把地面变为废土，地球变为地狱。
金字塔是制造这一切的充分必要条件，
这是几何学，难于否认也不需要废话。

塔上没有言论自由也就没有思想自由，
没有学术和信仰的自由，
所以这里没有创造力除了谎言和暴力，在这里，
忠孝礼义、仁义道德、温良恭俭... 都成了假货。

所谓悠久的历史其实是在朝代循环中退化的堕落。

集权的金字塔是实心的宝塔，塔内的人沦为假货的人。

在虚假的语言中我们无法谈什么平等与民主，
当文字都被污染之后也无法谈什么自然环境污染，
整个星球都将被毁灭。

然而这些爬虫们远逝的祖先并不知道有一个叫做中国的祖国可以把他们的后人变成这样。

这就是金字塔超越时空的力量，它来自地狱，它就是地狱的构造蓝图。

先辈们叹息：世无英雄乃使竖子成名。
祖先被奴役了，
才让祖国篡改出伟大的秽史，
秽史养育污秽的人，
良知变成狡猾，
舍是为了得，
糊涂变得难得，
可是人们还是变成了愚民，不可抗拒的，
正义和邪恶早成为了一对同义词。

不拆掉金字塔，就算是逃出地狱的人，
也不仅浑身散发着臭气，而且早晚要被拖回去。

当伪神和佛道儒家都跪在金字塔台阶上之后，
在秽史中，人们就忘记了人是什么？

是人民还是愚民？

是爬虫？还是地狱军团？

一群毒虫撕咬着堆积成为一座魔法塔，
金字塔是无敌的结构，屹立千年。

拆除金字塔的方法之一*可能是*尝试研究自己真实的历史。
每个人血脉的历史都超过 TA 祖国的。
祖国这个词是祖先的后人的不肖子孙发明的。
镇压人类的极权政府的历史相比每个人的来源不成比例的虚弱而且浅薄。

我们将在人类史中寻找一座座神坛，
一座座教堂在地图上一点点连成逃身的路线。
在重写历史的时候有机会澄清真相、恢复常识。
地狱中只有无神论，地狱生物厌恶「神」，
因此，顾及不幸堕落成为爬虫的人们我们不过多提及神学概念，
也仅仅使用逻辑和美学来帮助它们觉醒，
从而让它们有机会保全人体、仰望天堂。
有机会在科学知识和宗教伦理中祈祷救赎和自由，
在人类的学者眼中，科学与信仰绝不矛盾，
甚至原本就是同一类探索。

在金字塔眼中，
我们是万恶的逃亡者。
我们却也以为人类史就是逃亡史，
而不是地狱生物们所以为的胜利、胜利再胜利的不断追杀人类的胜利史，
这是为逃亡者写的书。

所以，中共、五毛、小粉红恶灵退散。

本书是以「[人类史和中国](history.and.china)」为基础改写的，
编辑工作刚开始，主要目的：
一是为了精简，让中学生在更短的时间内，比如一周内甚至一天内，可以读完。
二是为了探索在2020年中共瘟疫（武汉肺炎，COVID-19）重置全世界之后金字塔的拆解方法。

然而请注意，
历史不是写出来的，
如果是写出来的历史就至少是要给人们研究和证伪的，
而中共的历史是不敢让人来证伪的，
因为中共的历史和它们的极权爹妈们的史书就是金字塔的修造手册，
是地狱的工作法则、台阶上的升级宝典，
而且爬虫类只有以此为荣，以此为理想，
受教育、向上爬，否则就将被金字塔内的生活再次洗脑，
或者用酷刑或者活摘的方法消毒。

相反，逃亡者的最高荣誉就是为拆解地狱奉献哪怕微小的一份力量，
我们是人类，我们是同类，
逃离地狱路径和拆解地狱的方法须要公开而且安全的分享，
这些知识可以被研究，被测试，被证明和证伪，
逐渐提纯为知识中纯净的良知。



# 正文

[逃离极权金字塔](escape.pyramid)

